package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.dao.ProductRepository;
import com.twuc.webApp.domain.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import javax.persistence.EntityManager;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductTest extends TestBase {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private ObjectMapper objectMapper;
    @Test
    void should_return_200_when_add_product() throws Exception {

        Product product = new Product("kolo",new BigDecimal(12.0),"百事","http:local");

        String value = objectMapper.writeValueAsString(product);

        Product save = productRepository.save(product);
        entityManager.flush();
        entityManager.clear();

        assertEquals("百事", save.getUnit());
        getMockMvc().perform(post("/api/product").contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(value))
                .andExpect(status().isOk());

    }

    @Test
    void should_return_400_when_add_product() throws Exception {
        Product product = new Product();
        String value = objectMapper.writeValueAsString(product);
        getMockMvc().perform(post("/api/product").contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(value))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_when_get_all() throws Exception {
        getMockMvc().perform(get("/api/products").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }
//
//    @Test
//    void should_return_400_when_get_all() throws Exception {
//        getMockMvc().perform(get("/api/products").contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(status().isBadRequest());
//    }
}
