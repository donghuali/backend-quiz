package com.twuc.webApp.service;


import com.twuc.webApp.dao.OrderItemRepository;
import com.twuc.webApp.dao.OrderRepository;
import com.twuc.webApp.dao.ProductRepository;
import com.twuc.webApp.domain.Order;
import com.twuc.webApp.domain.Order_item;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Component
public class OrderItemService {

    @Autowired
    private OrderItemRepository orderItemRepository;


    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;


    public ResponseEntity getAllOrderItem() {

        List<Order_item> all = orderItemRepository.findAll();
        if (all != null) {
            return ResponseEntity.status(200)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(all);
        }
        return ResponseEntity.status(404).build();
    }


    public ResponseEntity createOrderItem(Long productId) {
        if (productRepository.existsById(productId)) {
            List<Order> all = orderRepository.findAll();
            Product product1 = productRepository.findById(productId).get();
            boolean res = orderItemRepository.existsByProduct(product1);
            if (!res) {
                Order_item order_item = new Order_item();
                order_item.setCount(1);
                order_item.setProduct(product1);
                Order_item save = orderItemRepository.save(order_item);


                return ResponseEntity.status(200).body(save);
            } else {
                Order_item byProduct = orderItemRepository.findByProduct(product1);
                byProduct.setCount(byProduct.getCount() + 1);
                Order_item save = orderItemRepository.save(byProduct);
                return ResponseEntity.status(200).body(save);
            }
        }
        return ResponseEntity.status(404).build();
    }

    private ResponseEntity getResponseEntity(Order order, Product product1, boolean res) {
        if(!res){
            Order_item order_item = new Order_item();
            order_item.setCount(1);
            order_item.setProduct(product1);

            ArrayList<Order_item> order_items = new ArrayList<>();
            order_items.add(order_item);
            order.setOrder_item(order_items);

            return ResponseEntity.status(200).body(order_item);
        }else{
            Order_item byProduct = orderItemRepository.findByProduct(product1);
            byProduct.setCount(byProduct.getCount()+1);
            return ResponseEntity.status(200).body(byProduct);
        }
    }

    public ResponseEntity deleteOrderItem(RequestParam requestParam) {
        orderItemRepository.deleteById(requestParam.getProductId());
        return ResponseEntity.status(200).build();
    }
}
