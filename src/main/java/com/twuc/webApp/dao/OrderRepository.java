package com.twuc.webApp.dao;


import com.twuc.webApp.domain.Order;
import com.twuc.webApp.domain.Order_item;
import com.twuc.webApp.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {

}
