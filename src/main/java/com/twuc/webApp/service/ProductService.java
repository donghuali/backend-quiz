package com.twuc.webApp.service;


import com.twuc.webApp.dao.ProductRepository;
import com.twuc.webApp.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Component
public class ProductService {

    @Autowired
    private ProductRepository productRepository;


    @Autowired
    private EntityManager entityManager;

    public ResponseEntity addProduct(Product product) {
        Product save = productRepository.save(product);
        return ResponseEntity.status(200).body(save);
    }

    public ResponseEntity getProducts() {
        List<Product> all = productRepository.findAll();

        if (all == null) {
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.status(200).body(all.stream());
    }
}
