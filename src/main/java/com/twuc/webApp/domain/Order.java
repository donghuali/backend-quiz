package com.twuc.webApp.domain;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;



    @OneToMany
    @JoinColumn(name = "order_id")
    private List<Order_item> order_item = new ArrayList<>();

    public Order(List<Order_item> order_item) {
        this.order_item = order_item;
    }

    public List<Order_item> getOrder_item() {
        return order_item;
    }

    public void setOrder_item(List<Order_item> order_item) {
        this.order_item = order_item;
    }

    public Order() {
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }

}
