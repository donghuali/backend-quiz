package com.twuc.webApp.dao;

import com.twuc.webApp.domain.Order_item;
import com.twuc.webApp.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderItemRepository extends JpaRepository<Order_item,Long> {


    Order_item findByProduct(Product product);

    boolean existsByProduct(Product product);

}
