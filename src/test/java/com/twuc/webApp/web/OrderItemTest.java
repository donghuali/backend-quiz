package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.dao.OrderItemRepository;
import com.twuc.webApp.dao.OrderRepository;
import com.twuc.webApp.dao.ProductRepository;
import com.twuc.webApp.domain.Order;
import com.twuc.webApp.domain.Order_item;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.RequestParam;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OrderItemTest extends TestBase {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private ProductRepository productRepository;
    @Test
    void should_return_200_get_all_order() throws Exception {

        Product product = new Product("kele", new BigDecimal(12), "个", "http://loca");
        Product save = productRepository.save(product);

        Order_item order_item = new Order_item(save, 1);
        Order_item save1 = orderItemRepository.save(order_item);

        List<Order_item> all = orderItemRepository.findAll();
        assertEquals(1, all.size());
        getMockMvc().perform(get("/api/order_item").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_200_create_order_item() throws Exception {
        Product product = new Product("name", new BigDecimal(12), "nam", "http://loca");
        Product save = productRepository.save(product);

        RequestParam requestParam = new RequestParam(save.getId());
        String value = objectMapper.writeValueAsString(requestParam);

        List<Order_item> all = orderItemRepository.findAll();
        assertEquals(1, all.size());

        getMockMvc().perform(post("/api/create/order_item").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(value))
                .andExpect(status().isOk());
    }





    @Test
    void should_create_order_fail_with_not_productId() throws Exception {
        RequestParam requestParam = new RequestParam(1l);
        String value = objectMapper.writeValueAsString(requestParam);
        getMockMvc().perform(post("/api/create/order").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(value))
                .andExpect(status().isNotFound());
    }
}
