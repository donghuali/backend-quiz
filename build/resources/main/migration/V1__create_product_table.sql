create table if not exists Product(
    id BIGINT not null primary key ,
    name VARCHAR(128) not null ,
    price BIGINT not null ,
    unit VARCHAR(128) not null,
    img VARCHAR(250) not null
)


