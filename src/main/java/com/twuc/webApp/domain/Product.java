package com.twuc.webApp.domain;


import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;


    @Column(name = "name",nullable = false)
    @Length(max = 128)
    @NotNull
    private String name;

    @Column(name = "price",nullable = false)
    @NotNull
    private BigDecimal price;


    @NotNull
    @Column(name = "unit",nullable = false)
    @Length(max = 128)
    private String unit;

    @NotNull
    @Column(name = "img",nullable = false)
    private String img;

    public Product() {
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Product(@Length(max = 128) @NotNull String name, @NotNull BigDecimal price, @NotNull @Length(max = 128) String unit, @NotNull String img) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.img = img;
    }

}
