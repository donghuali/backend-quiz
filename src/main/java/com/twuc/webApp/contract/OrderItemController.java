package com.twuc.webApp.contract;


import com.twuc.webApp.domain.RequestParam;
import com.twuc.webApp.service.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class OrderItemController {


    @Autowired
    private OrderItemService orderItemService;

    @GetMapping("/order_item")
    public ResponseEntity getAllOrderItem() {
       return orderItemService.getAllOrderItem();
    }

    @PostMapping("/create/order_item")
    public ResponseEntity createOrder_item(@RequestBody RequestParam requestParam) {
        return orderItemService.createOrderItem(requestParam.getProductId());
    }

    @DeleteMapping("/delete/order_item")
    public ResponseEntity deleteOrder(@RequestBody RequestParam requestParam){
        return orderItemService.deleteOrderItem(requestParam);
    }

}
