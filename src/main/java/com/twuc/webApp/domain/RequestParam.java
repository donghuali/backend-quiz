package com.twuc.webApp.domain;


public class RequestParam {

    private Long productId;

    public Long getProductId() {
        return productId;
    }

    public RequestParam(Long productId) {
        this.productId = productId;
    }

    public RequestParam() {
    }
}
